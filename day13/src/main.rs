use std::collections::{BTreeMap, BTreeSet};
const INPUT: &str = include_str!("input.txt");

type Rel = BTreeMap<(String, String), i32>;
type People = BTreeSet<String>;

fn load(input: &str) -> (Rel, People) {
    let mut rel = Rel::new();
    let mut ppl = People::new();

    for line in input.trim().lines() {
        match line.split(' ').collect::<Vec<_>>()[..] {
            [a, "would", sign, amount, "happiness", "units", "by", "sitting", "next", "to", b] => {
                let sign = match sign {
                    "gain" => 1,
                    "lose" => -1,
                    _ => panic!(
                        "invalid input: expected 'gain' or 'lose', but got '{}'",
                        sign
                    ),
                };
                let amount = match amount.parse::<i32>() {
                    Ok(num) => num,
                    _ => panic!("invalid input: amount of happiness change is not a number"),
                };
                ppl.insert(a.to_string());
                // assume the last char is a full-stop
                ppl.insert(b.trim_end_matches('.').to_string());
                rel.insert(
                    (a.to_string(), b.trim_end_matches('.').to_string()),
                    sign * amount,
                );
            }
            _ => panic!("invalid input: unrecognizable sentence structure"),
        }
    }

    (rel, ppl)
}

fn main() {
    use itertools::Itertools;
    let (mut rel, mut ppl) = load(INPUT);
    println!(
        "Part 1: {:?}",
        ppl.iter()
            .permutations(ppl.len())
            .map(|ring| {
                let mut happiness = 0;
                let n = ring.len();
                for i in 0..n {
                    happiness += rel
                        .get(&(ring[i].to_string(), ring[(n + i - 1) % n].to_string()))
                        .expect("a");
                    happiness += rel
                        .get(&(ring[i].to_string(), ring[(i + 1) % n].to_string()))
                        .expect("b");
                }
                happiness
            })
            .max()
            .unwrap()
    );

    ppl.insert("Eric Wastl".into());
    for person in &ppl {
        rel.insert((person.clone(), "Eric Wastl".into()), 0);
        rel.insert(("Eric Wastl".into(), person.clone()), 0);
    }

    println!(
        "Part 2: {:}",
        ppl.iter()
            .permutations(ppl.len())
            .map(|ring| {
                let mut happiness = 0;
                let n = ring.len();
                for i in 0..n {
                    happiness += rel
                        .get(&(ring[i].to_string(), ring[(n + i - 1) % n].to_string()))
                        .expect("a");
                    happiness += rel
                        .get(&(ring[i].to_string(), ring[(i + 1) % n].to_string()))
                        .expect("b");
                }
                happiness
            })
            .max()
            .unwrap()
    );
}
