const INPUT: &str = include_str!("input.txt");

fn valid(password: &[u8]) -> bool {
    password
        .windows(3)
        .find(|w| w[0] + 1 == w[1] && w[1] + 1 == w[2])
        .is_some()
        && password
            .iter()
            .find(|c| vec!['i', 'o', 'l'].contains(&(**c as char)))
            .is_none()
        // below is a bug for when we have two number pairs which consist of different digits
        && {
            let pairs: Vec<_> = password.windows(2).enumerate().filter(|(_,w)| w[0] == w[1]).map(|(i,_)| i).collect();
            pairs.len() > 2 || (pairs.len() == 2 && pairs[1] > pairs[0] + 1)
        }
}

fn next(password: &mut Vec<u8>) {
    if password.is_empty() {
        password.push('a' as u8);
    } else {
        let mut i = password.len();

        while i > 0 && password[i - 1] == ('z' as u8) {
            i -= 1;
            password[i] = 'a' as u8;
        }

        if i > 0 {
            password[i - 1] += 1;
        }
    }
}

fn next_valid(password: &mut Vec<u8>) {
    next(password);
    while !valid(password) {
        next(password);
    }
}

fn main() {
    let mut password = INPUT.trim().as_bytes().to_vec();
    next_valid(&mut password);
    println!("Part 1: {}", String::from_utf8(password.clone()).unwrap());
    next_valid(&mut password);
    println!("Part 2: {}", String::from_utf8(password).unwrap());
}
