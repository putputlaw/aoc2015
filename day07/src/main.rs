use std::collections::BTreeMap;

const INPUT: &str = include_str!("input.txt");

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Node {
    Val(Val),
    And(Val, Val),
    Or(Val, Val),
    LShift(Val, Val),
    RShift(Val, Val),
    Not(Val),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Val {
    Ref(String),
    Raw(u16),
}

impl Val {
    fn new(s: &str) -> Self {
        match s.parse::<u16>() {
            Ok(num) => Val::Raw(num),
            Err(_) => Val::Ref(s.to_string()),
        }
    }
}

type Deps = BTreeMap<String, Node>;

fn dependencies(input: &str) -> Deps {
    let mut tree = BTreeMap::new();
    for line in input.trim().lines() {
        match &line.split(' ').collect::<Vec<_>>()[..] {
            [a, op, b, "->", c] => {
                let a = Val::new(a);
                let b = Val::new(b);
                let c = c.to_string();
                let node = match *op {
                    "AND" => Node::And(a, b),
                    "OR" => Node::Or(a, b),
                    "LSHIFT" => Node::LShift(a, b),
                    "RSHIFT" => Node::RShift(a, b),
                    _ => panic!("unknown binary operation {:?}", op),
                };
                tree.insert(c, node);
            }
            [op, a, "->", c] => {
                let a = Val::new(a);
                let c = c.to_string();
                let node = match *op {
                    "NOT" => Node::Not(a),
                    _ => panic!("unknown unary operation {:?}", op),
                };
                tree.insert(c, node);
            }
            [a, "->", c] => {
                let a = Val::new(a);
                let c = c.to_string();
                let node = Node::Val(a);
                tree.insert(c, node);
            }
            _ => panic!("invalid input"),
        }
    }
    tree
}

fn resolve(d: &mut Deps, key: &Val) -> u16 {
    match key {
        Val::Raw(a) => *a,
        Val::Ref(key) => {
            let mut ans = 0;
            let node = d.get(key).cloned();
            if let Some(node) = node {
                match node {
                    Node::Val(Val::Raw(a)) => {
                        ans = a;
                    }
                    Node::Val(reference) => {
                        let a = resolve(d, &reference);
                        ans = a;
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                    Node::And(a, b) => {
                        let a = resolve(d, &a);
                        let b = resolve(d, &b);
                        ans = a & b;
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                    Node::Or(a, b) => {
                        let a = resolve(d, &a);
                        let b = resolve(d, &b);
                        ans = a | b;
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                    Node::LShift(a, b) => {
                        let a = resolve(d, &a);
                        let b = resolve(d, &b);
                        ans = a << b;
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                    Node::RShift(a, b) => {
                        let a = resolve(d, &a);
                        let b = resolve(d, &b);
                        ans = a >> b;
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                    Node::Not(a) => {
                        let a = resolve(d, &a);
                        ans = a ^ u16::max_value();
                        d.insert(key.to_string(), Node::Val(Val::Raw(ans)));
                    }
                }
            }
            ans
        }
    }
}

fn main() {
    let mut deps = dependencies(INPUT);
    let val = resolve(&mut deps, &Val::Ref("a".into()));
    println!("Part 1: {:?}", val);
    let mut deps = dependencies(INPUT);
    deps.insert("b".to_string(), Node::Val(Val::Raw(val)));
    let val = resolve(&mut deps, &Val::Ref("a".into()));
    println!("Part 2: {:?}", val);
}
