const INPUT: &str = include_str!("input.txt");

fn main() {
    println!(
        "Part 1: {}",
        INPUT.trim().chars().fold(0i32, |acc, c| {
            acc + match c {
                '(' => 1,
                ')' => -1,
                _ => panic!("invalid input"),
            }
        })
    );

    println!(
        "Part 1: {}",
        INPUT
            .trim()
            .chars()
            .enumerate()
            .fold((0i32, None), |(acc, found), (i, c)| {
                let acc = acc
                    + match c {
                        '(' => 1,
                        ')' => -1,
                        _ => panic!("invalid input"),
                    };
                let found = if acc == -1 { found.or(Some(i + 1)) } else { found };
                (acc, found)
            })
            .1
            .expect("never entered the basement")
    );
}
