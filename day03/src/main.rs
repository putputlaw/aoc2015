const INPUT: &str = include_str!("input.txt");

type Pos = (i32, i32);

fn parse(input: &str) -> Vec<Pos> {
    input.trim().chars().fold(vec![(0, 0)], |mut path, c| {
        if let Some(&(x, y)) = path.last() {
            let next = match c {
                '^' => (x, y + 1),
                'v' => (x, y - 1),
                '<' => (x - 1, y),
                '>' => (x + 1, y),
                _ => panic!("invalid input"),
            };
            path.push(next);
        }
        path
    })
}

fn parse2(input: &str) -> Vec<Vec<Pos>> {
    input
        .trim()
        .chars()
        .enumerate()
        .fold(vec![vec![(0, 0)], vec![(0, 0)]], |mut paths, (i, c)| {
            let path = &mut paths[i % 2];
            if let Some(&(x, y)) = path.last() {
                let next = match c {
                    '^' => (x, y + 1),
                    'v' => (x, y - 1),
                    '<' => (x - 1, y),
                    '>' => (x + 1, y),
                    _ => panic!("invalid input"),
                };
                path.push(next);
            }
            paths
        })
}

fn main() {
    use std::collections::BTreeSet;

    println!(
        "Part 1: {}",
        parse(INPUT).into_iter().collect::<BTreeSet<Pos>>().len()
    );

    println!(
        "Part 2: {}",
        parse2(INPUT)
            .into_iter()
            .map(|path| path.into_iter().collect::<BTreeSet<Pos>>())
            .fold(BTreeSet::new(), |u, p| u.union(&p).cloned().collect())
            .len()
    );
}
