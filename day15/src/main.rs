use std::collections::BTreeMap;

const INPUT: &str = include_str!("input.txt");

#[derive(Debug, Clone)]
struct IngredientSpec {
    capacity: i64,
    durability: i64,
    flavour: i64,
    texture: i64,
    calories: i64,
}

impl IngredientSpec {
    fn scale(&self, s: i64) -> Self {
        Self {
            capacity: s * self.capacity,
            durability: s * self.durability,
            flavour: s * self.flavour,
            texture: s * self.texture,
            calories: s * self.calories,
        }
    }

    fn add(&self, other: &Self) -> Self {
        Self {
            capacity: self.capacity + other.capacity,
            durability: self.durability + other.durability,
            flavour: self.flavour + other.flavour,
            texture: self.texture + other.texture,
            calories: self.calories + other.calories,
        }
    }

    fn zero() -> Self {
        Self {
            capacity: 0,
            durability: 0,
            flavour: 0,
            texture: 0,
            calories: 0,
        }
    }
}

fn ingredients(input: &str) -> BTreeMap<String, IngredientSpec> {
    fn parse_num(num: &str) -> i64 {
        num.trim_end_matches(',').parse().unwrap()
    }

    input.trim().lines().map(|line| {
        match line.split(' ').collect::<Vec<_>>()[..] {
            [ingredient, "capacity", capacity, "durability", durability, "flavor", flavour, "texture", texture, "calories", calories] => {
                let ingredient = ingredient.trim_end_matches(':').to_string();
                let capacity = parse_num(capacity);
                let durability = parse_num(durability);
                let flavour = parse_num(flavour);
                let texture = parse_num(texture);
                let calories = parse_num(calories);
                (ingredient, IngredientSpec {capacity, durability, flavour, texture, calories})
            }
            _ => panic!("invalid input"),
        }
    }).collect()
}

fn score(
    specs: &BTreeMap<String, IngredientSpec>,
    recipe: &BTreeMap<String, usize>,
    calory_constraint: Option<i64>,
) -> i64 {
    let IngredientSpec {
        capacity,
        durability,
        flavour,
        texture,
        calories,
    } = recipe
        .iter()
        .map(|(ingredient, amount)| {
            let spec = specs.get(&ingredient.to_string()).unwrap();
            spec.scale(*amount as i64)
        })
        .fold(IngredientSpec::zero(), |a, b| a.add(&b));

    match calory_constraint {
        Some(val) if val != calories => 0,
        _ => capacity.max(0) * durability.max(0) * flavour.max(0) * texture.max(0),
    }
}

fn main() {
    let specs = ingredients(INPUT.trim());
    let mut max = 0;

    for i in 0..=100 {
        for j in 0..=100 - i {
            for k in 0..=100 - i - j {
                let l = 100 - i - j - k;
                // TODO: works for me! ;)
                let recipe = vec![
                    ("Sprinkles".into(), i),
                    ("Butterscotch".into(), j),
                    ("Chocolate".into(), k),
                    ("Candy".into(), l),
                ]
                .into_iter()
                .collect();

                max = max.max(score(&specs, &recipe, None));
            }
        }
    }

    println!("Part 1: {}", max);

    let specs = ingredients(INPUT.trim());
    let mut max = 0;

    for i in 0..=100 {
        for j in 0..=100 - i {
            for k in 0..=100 - i - j {
                let l = 100 - i - j - k;
                let recipe = vec![
                    ("Sprinkles".into(), i),
                    ("Butterscotch".into(), j),
                    ("Chocolate".into(), k),
                    ("Candy".into(), l),
                ]
                .into_iter()
                .collect();

                max = max.max(score(&specs, &recipe, Some(500)));
            }
        }
    }

    println!("Part 2: {}", max);
}
