const INPUT: &str = include_str!("input.txt");

#[derive(Clone)]
struct Grid {
    width: usize,
    height: usize,
    contents: Vec<bool>,
}

impl Grid {
    fn new(width: usize, height: usize) -> Self {
        Grid {
            width,
            height,
            contents: vec![false; 100 * 100],
        }
    }

    fn build<F: Fn(usize, usize) -> bool>(width: usize, height: usize, f: F) -> Self {
        let mut grid = Self::new(width, height);
        for i in 0..grid.height {
            for j in 0..grid.width {
                grid.set(i, j, f(i, j))
            }
        }
        grid
    }

    fn get(&self, row: usize, col: usize) -> Option<bool> {
        if (0..self.height).contains(&row) && (0..self.width).contains(&col) {
            Some(self.contents[row * self.width + col])
        } else {
            None
        }
    }

    fn set(&mut self, row: usize, col: usize, value: bool) {
        if (0..self.height).contains(&row) && (0..self.width).contains(&col) {
            self.contents[row * self.width + col] = value
        }
    }

    fn neighbourhood_score(&self, row: usize, col: usize) -> usize {
        let mut neighbours = vec![(row + 1, col), (row, col + 1), (row + 1, col + 1)];
        if row > 0 {
            neighbours.push((row - 1, col));
            neighbours.push((row - 1, col + 1));
        }
        if col > 0 {
            neighbours.push((row, col - 1));
            neighbours.push((row + 1, col - 1));
        }
        if row > 0 && col > 0 {
            neighbours.push((row - 1, col - 1));
        }
        neighbours
            .into_iter()
            .flat_map(|(i, j)| self.get(i, j))
            .filter(|x| *x)
            .count()
    }

    fn parse(width: usize, height: usize, input: &str) -> Self {
        let raw = input
            .trim()
            .lines()
            .map(|line| line.chars().map(|c| c == '#').collect::<Vec<_>>())
            .collect::<Vec<_>>();
        Grid::build(width, height, |i, j| raw[i][j])
    }

    fn evolve(&mut self) -> Self {
        Grid::build(self.width, self.height, |i, j| {
            let score = self.neighbourhood_score(i, j);
            if self.get(i, j).unwrap() {
                score == 2 || score == 3
            } else {
                score == 3
            }
        })
    }

    fn count(&self) -> usize {
        self.contents.iter().filter(|x| **x).count()
    }
}

fn main() {
    let mut grid = Grid::parse(100, 100, INPUT);
    for _ in 0..100 {
        grid = grid.evolve();
    }
    println!("Part 1: {}", grid.count());

    let mut grid = Grid::parse(100, 100, INPUT);
    grid.set(0, 0, true);
    grid.set(99, 0, true);
    grid.set(0, 99, true);
    grid.set(99, 99, true);
    for _ in 0..100 {
        grid = grid.evolve();
        grid.set(0, 0, true);
        grid.set(99, 0, true);
        grid.set(0, 99, true);
        grid.set(99, 99, true);
    }
    println!("Part 2: {}", grid.count());
}
