const INPUT: &str = include_str!("input.txt");

// parse integer triples and **sort** them
fn parse(input: &str) -> Vec<(i32, i32, i32)> {
    input
        .trim()
        .lines()
        .map(|line| {
            let mut ints = line
                .split('x')
                .flat_map(|val| val.parse::<i32>())
                .collect::<Vec<_>>();
            ints.sort();
            match &ints[..] {
                [a, b, c] => (*a, *b, *c),
                _ => panic!("failed to parse triple"),
            }
        })
        .collect()
}

fn main() {
    println!(
        "Part 1: {}",
        parse(INPUT)
            .iter()
            .map(|(a, b, c)| { 2 * (a * b + b * c + c * a) + a * b })
            .sum::<i32>()
    );

    println!(
        "Part 2: {}",
        parse(INPUT)
            .iter()
            .map(|(a, b, c)| { 2 * (a + b) + a * b * c })
            .sum::<i32>()
    );
}
