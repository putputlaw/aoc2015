use std::collections::BTreeMap;

const INPUT: &str = include_str!("input.txt");

type ReindeerSpec = (usize, usize, usize);

fn reindeers(input: &str) -> BTreeMap<String, ReindeerSpec> {
    input.lines().map(|line| {
        match line.split(' ').collect::<Vec<_>>()[..] {
            [a, "can", "fly", s, "km/s", "for", d, "seconds,", "but", "then", "must", "rest", "for", r, "seconds."] => {
                (a.to_string(), (s.parse().unwrap(), d.parse().unwrap(), r.parse().unwrap()))
            },
            _ => panic!("invalid input: {}", line),
        }
    }).collect()
}

fn travel((s, d, r): ReindeerSpec, time: usize) -> usize {
    let period = d + r;
    s * ((time / period) * d + std::cmp::min(d, time % period))
}

fn main() {
    let spec = reindeers(INPUT);

    println!(
        "Part 1: {}",
        spec.iter()
            .map(|(name, v)| (name, travel(*v, 2503)))
            .max_by_key(|(_, v)| *v)
            .unwrap()
            .1
    );

    let mut points: BTreeMap<_, _> = spec.iter().map(|(name, _)| (name.to_string(), 0)).collect();
    for round in 1..=2503 {
        let lead = spec
            .iter()
            .map(|(name, v)| (name, travel(*v, round)))
            .map(|(_, v)| v)
            .max()
            .unwrap();

        spec.iter()
            .filter(|(_, v)| travel(**v, round) == lead)
            .for_each(|(name, _)| {
                points.entry(name.clone()).and_modify(|v| *v += 1);
            });
    }

    println!("Part 2: {}", points.values().max().unwrap());
}
