use std::collections::BTreeMap;
use std::cmp::{Ordering, Ordering::*};

const INPUT: &str = include_str!("input.txt");

type GrannySpec = BTreeMap<String, usize>;

fn grannys(input: &str) -> BTreeMap<usize, GrannySpec> {
    input
        .lines()
        .map(|line| match line.split(' ').collect::<Vec<_>>()[..] {
            ["Sue", granny, kw1, num1, kw2, num2, kw3, num3] => {
                let granny = granny.trim_end_matches(':').parse().unwrap();
                let kw1 = kw1.trim_end_matches(':').to_string();
                let num1 = num1.trim_end_matches(',').parse().unwrap();
                let kw2 = kw2.trim_end_matches(':').to_string();
                let num2 = num2.trim_end_matches(',').parse().unwrap();
                let kw3 = kw3.trim_end_matches(':').to_string();
                let num3 = num3.trim_end_matches(',').parse().unwrap();
                let mut spec = GrannySpec::new();
                spec.insert(kw1, num1);
                spec.insert(kw2, num2);
                spec.insert(kw3, num3);
                (granny, spec)
            }
            _ => panic!("invalid input"),
        })
        .collect()
}

fn is_consistent_with(spec: &GrannySpec, other: &GrannySpec) -> bool {
    spec.iter()
        .all(|(k, v)| other.get(k).map(|ov| *v == *ov).unwrap_or(false))
}


type GrannySpec2 = BTreeMap<String, (Ordering, usize)>;

fn is_consistent_with2(spec: &GrannySpec, other: &GrannySpec2) -> bool {
    spec.iter()
        .all(|(k, v)| other.get(k).map(|(cmp, ov)| v.cmp(ov) == *cmp).unwrap_or(false))
}


fn main() {
    let master_spec: GrannySpec = vec![
        ("children", 3),
        ("cats", 7),
        ("samoyeds", 2),
        ("pomeranians", 3),
        ("akitas", 0),
        ("vizslas", 0),
        ("goldfish", 5),
        ("trees", 3),
        ("cars", 2),
        ("perfumes", 1),
    ]
    .into_iter()
    .map(|(k, v)| (k.to_string(), v))
    .collect();

    println!(
        "Part 1: {}",
        grannys(INPUT)
            .into_iter()
            .find(|(_, spec)| is_consistent_with(&spec, &master_spec))
            .unwrap()
            .0
    );


    let master_spec2: GrannySpec2 = vec![
        ("children", (Equal, 3)),
        ("cats", (Greater, 7)),
        ("samoyeds", (Equal, 2)),
        ("pomeranians", (Less, 3)),
        ("akitas", (Equal, 0)),
        ("vizslas", (Equal, 0)),
        ("goldfish", (Less, 5)),
        ("trees", (Greater, 3)),
        ("cars", (Equal, 2)),
        ("perfumes", (Equal, 1)),
    ]
    .into_iter()
    .map(|(k, v)| (k.to_string(), v))
    .collect();

    println!(
        "Part 2: {}",
        grannys(INPUT)
            .into_iter()
            .find(|(_, spec)| is_consistent_with2(&spec, &master_spec2))
            .unwrap()
            .0
    );
}
