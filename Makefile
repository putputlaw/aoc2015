##
# Cleanup Squad
#
# @file
# @version 0.1

.PHONY: clean

clean:
	rm day*/target -rf
	rm aoc-utils/target -rf

# end
