const INPUT: &str = include_str!("input.txt");

struct Grid(Vec<Vec<bool>>);

impl Grid {
    fn new() -> Self {
        Self(
            (0..1000)
                .map(|_| (0..1000).map(|_| false).collect())
                .collect(),
        )
    }

    fn on(&mut self, i: usize, j: usize) {
        self.0[i][j] = true;
    }

    fn off(&mut self, i: usize, j: usize) {
        self.0[i][j] = false;
    }

    fn toggle(&mut self, i: usize, j: usize) {
        self.0[i][j] = !self.0[i][j];
    }

    fn count(&self) -> usize {
        self.0
            .iter()
            .map(|col| col.iter().filter(|e| **e).count())
            .sum::<usize>()
    }
}

fn parse_range(from: &str, to: &str) -> (usize, usize, usize, usize) {
    let froms: Vec<_> = from
        .split(',')
        .flat_map(|num| num.parse::<usize>())
        .collect();
    let tos: Vec<_> = to.split(',').flat_map(|num| num.parse::<usize>()).collect();
    (froms[0], froms[1], tos[0], tos[1])
}

fn part1() {
    let mut grid = Grid::new();
    for line in INPUT.trim().lines() {
        match &line.split(' ').collect::<Vec<_>>()[..] {
            ["turn", "on", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.on(i, j);
                    }
                }
            }
            ["toggle", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.toggle(i, j);
                    }
                }
            }
            ["turn", "off", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.off(i, j);
                    }
                }
            }
            _ => panic!("invalid input"),
        }
    }
    println!("Part 1: {}", grid.count());
}

struct Grid2(Vec<Vec<usize>>);

impl Grid2 {
    fn new() -> Self {
        Self((0..1000).map(|_| (0..1000).map(|_| 0).collect()).collect())
    }

    fn on(&mut self, i: usize, j: usize) {
        self.0[i][j] += 1;
    }

    fn off(&mut self, i: usize, j: usize) {
        self.0[i][j] = self.0[i][j].saturating_sub(1);
    }

    fn toggle(&mut self, i: usize, j: usize) {
        self.0[i][j] += 2;
    }

    fn count(&self) -> usize {
        self.0
            .iter()
            .map(|col| col.iter().sum::<usize>())
            .sum::<usize>()
    }
}

fn part2() {
    let mut grid = Grid2::new();
    for line in INPUT.trim().lines() {
        match &line.split(' ').collect::<Vec<_>>()[..] {
            ["turn", "on", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.on(i, j);
                    }
                }
            }
            ["toggle", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.toggle(i, j);
                    }
                }
            }
            ["turn", "off", x, "through", y] => {
                let (imin, jmin, imax, jmax) = parse_range(x, y);
                for i in imin..=imax {
                    for j in jmin..=jmax {
                        grid.off(i, j);
                    }
                }
            }
            _ => panic!("invalid input"),
        }
    }
    println!("Part 2: {}", grid.count());
}

fn main() {
    part1();
    part2();
}
