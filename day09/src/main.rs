use std::collections::{BTreeMap, BTreeSet};

const INPUT: &str = include_str!("input.txt");

type Distances = BTreeMap<(String, String), usize>;
type Locations = BTreeSet<String>;

fn parse(input: &str) -> (Distances, Locations) {
    let mut distances = BTreeMap::new();
    let mut locations = BTreeSet::new();
    input
        .trim()
        .lines()
        .for_each(|line| match line.split(' ').collect::<Vec<_>>()[..] {
            [source, "to", destination, "=", dist] => {
                locations.insert(source.to_string());
                locations.insert(destination.to_string());
                distances.insert(
                    (source.to_string(), destination.to_string()),
                    dist.parse().unwrap(),
                );
                distances.insert(
                    (destination.to_string(), source.to_string()),
                    dist.parse().unwrap(),
                );
            }
            _ => panic!("invalid input"),
        });
    (distances, locations)
}

fn main() {
    use itertools::Itertools;
    let (distances, locations) = parse(INPUT);
    println!(
        "Part 1: {:?}",
        locations
            .iter()
            .permutations(locations.len())
            .map(|path| {
                path.windows(2)
                    .flat_map(|w| distances.get(&(w[0].to_string(), w[1].to_string())))
                    .sum::<usize>()
            })
            .min()
            .unwrap()
    );

    println!(
        "Part 1: {:?}",
        locations
            .iter()
            .permutations(locations.len())
            .map(|path| {
                path.windows(2)
                    .flat_map(|w| distances.get(&(w[0].to_string(), w[1].to_string())))
                    .sum::<usize>()
            })
            .max()
            .unwrap()
    );
}
