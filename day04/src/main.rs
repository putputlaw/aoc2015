const INPUT: &str = include_str!("input.txt");

fn ok(secret_key: &str, prefix_len: usize, num: u32) -> bool {
    format!(
        "{:x}",
        md5::compute(format!("{}{}", secret_key, num).as_bytes())
    )
    .chars()
    .take(prefix_len)
    .all(|c| c == '0')
}

fn main() {
    println!(
        "Part 1: {}",
        (1..).find(|num| ok(INPUT.trim(), 5, *num)).unwrap()
    );

    println!(
        "Part 2: {}",
        (1..).find(|num| ok(INPUT.trim(), 6, *num)).unwrap()
    );
}
