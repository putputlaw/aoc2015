use std::collections::*;

const INPUT: &str = include_str!("input.txt");
const EGG_NOGG_VOLUME: i32 = 150;

type Container = i32;

fn parse_containers(input: &str) -> Vec<Container> {
    input
        .trim()
        .lines()
        .map(|line| line.parse().expect("invalid input"))
        .collect()
}


fn main() {
    let max_volume = EGG_NOGG_VOLUME;
    let input = INPUT;

    // we use a dynamic programming approach

    let containers = parse_containers(input);
    let mut num_partitions = BTreeMap::new();
    num_partitions.insert(0, 1);
    let mut max = 0;
    for c in containers {
        max += c;
        // NOTE: minor optimization: partitions above 150 don't interest us
        for volume in (0..=max.min(max_volume)).rev() {
            num_partitions.insert(
                volume,
                num_partitions.get(&volume).unwrap_or(&0)
                    + num_partitions.get(&(volume - c)).unwrap_or(&0),
            );
        }
    }

    println!("Part 1: {}", num_partitions.get(&max_volume).unwrap());

    let containers = parse_containers(input);
    let mut num_partitions = BTreeMap::new();
    num_partitions.insert((0, 0), 1);
    let mut max = 0;
    for (i, c) in containers.into_iter().enumerate() {
        let i = i as i32;
        max += c;
        // NOTE: minor optimization: partitions above 150 don't interest us
        for volume in (0..=max.min(max_volume)).rev() {
            for num_containers in (0..=i).rev() {
                num_partitions.insert(
                    (num_containers + 1, volume),
                    num_partitions.get(&(num_containers + 1, volume)).unwrap_or(&0)
                        + num_partitions
                            .get(&(num_containers, volume - c))
                            .unwrap_or(&0),
                );
            }
        }
    }

    println!(
        "Part 2: {:?}",
        num_partitions
            .into_iter()
            .filter(|((_,volume), count)| *volume == max_volume && *count > 0)
            .min_by_key(|((num_containers,_), _)| *num_containers)
            .unwrap()
            .1
    );
}
