const INPUT: &str = include_str!("input.txt");

fn parse(input: &str) -> Vec<&str> {
    input.trim().lines().collect()
}

const VOWELS: &[char] = &['a', 'e', 'i', 'o', 'u'];

const FORBIDDEN: &[&[u8]] = &[&[b'a', b'b'], &[b'c', b'd'], &[b'p', b'q'], &[b'x', b'y']];

fn nice(input: &str) -> bool {
    let contains_three_vowels = input.chars().filter(|c| VOWELS.contains(c)).count() >= 3;
    let has_double_letter = input.as_bytes().windows(2).find(|w| w[0] == w[1]).is_some();
    let no_forbidden_strings = input
        .as_bytes()
        .windows(2)
        .find(|w| FORBIDDEN.contains(w))
        .is_none();
    contains_three_vowels && has_double_letter && no_forbidden_strings
}

fn nice2(input: &str) -> bool {
    use std::collections::BTreeMap;
    let mut pairs = BTreeMap::new();
    input
        .as_bytes()
        .windows(2)
        .enumerate()
        .for_each(|(i, w)| pairs.entry(w).or_insert(vec![]).push(i));
    let has_non_overlapping_duplicate_pairs = pairs
        .values()
        // either we have 3 or more identical pairs, so two of them automatically don't overlap
        // or we have exactly two, and we have to check for overlap
        .find(|ix| ix.len() > 2 || (ix.len() == 2 && ix[1] > 1 + ix[0]))
        .is_some();
    let has_aba_pattern = input.as_bytes().windows(3).find(|w| w[0] == w[2]).is_some();
    has_non_overlapping_duplicate_pairs && has_aba_pattern
}

fn main() {
    println!(
        "Part 1: {:?}",
        parse(INPUT).into_iter().filter(|s| nice(s)).count()
    );

    println!(
        "Part 2: {:?}",
        parse(INPUT).into_iter().filter(|s| nice2(s)).count()
    );

    println!("{:?}", "xxyxx".as_bytes().windows(2).collect::<Vec<_>>());
}

#[test]
fn test() {
    assert!(nice2("qjhvhtzxzqqjkmpb"));
    assert!(nice2("xxyxx"));
    assert!(!nice2("uurcxstgmygtbstg"));
    assert!(!nice2("ieodomkazucvgmuy"));
    assert!(!nice2("aaa"));
}
