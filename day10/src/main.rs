const INPUT: &str = include_str!("input.txt");

fn round(input: &str) -> String {
    use itertools::Itertools;
    input.chars().group_by(|e| *e).into_iter().map(|(h, it)| {
        format!("{}{}", it.count(), h)
    }).join("")
}

fn main() {
    let mut seq = INPUT.trim().to_string();
    for _ in 0..40 {
        seq = round(&seq);
    }
    println!("Part 1: {}", seq.len());

    for _ in 0..10 {
        seq = round(&seq);
    }
    println!("Part 2: {}", seq.len());
}
