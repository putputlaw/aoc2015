const INPUT: &str = include_str!("input.txt");

fn naive_consume(input: &str) -> i32 {
    let mut sum = 0;
    let mut num = vec![];
    let mut chars = input.chars().peekable();
    while let Some(c) = chars.next() {
        match c {
            _ if c == '-' && num.is_empty() => {
                num.push(c as u8);
            }
            _ if '0' <= c && c <= '9' => {
                num.push(c as u8);
            }
            _ if !num.is_empty() => {
                sum += String::from_utf8_lossy(&num).parse::<i32>().unwrap();
                num = vec![];
            }
            _ => {}
        }
    }
    if !num.is_empty() {
        sum += String::from_utf8_lossy(&num).parse::<i32>().unwrap();
    }
    sum
}

fn proper_consume(input: &str) -> i32 {
    use json::{parse, JsonValue, JsonValue::*};
    let doc = parse(input).unwrap();

    fn dive(val: &JsonValue) -> i32 {
        match val {
            Number(num) => i32::from(*num),
            Array(vec) => vec.into_iter().map(dive).sum::<i32>(),
            Object(obj) => obj
                .iter()
                .map(|(_, child)| match child {
                    Short(s) if s.as_str() == "red" => None,
                    String(s) if s.as_str() == "red" => None,
                    _ => Some(dive(child)),
                })
                .sum::<Option<i32>>()
                .unwrap_or(0),
            _ => 0,
        }
    }

    dive(&doc)
}

fn main() {
    println!("Part 1: {}", naive_consume(INPUT));
    println!("Part 2: {}", proper_consume(INPUT));
}
