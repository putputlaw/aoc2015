const INPUT: &str = include_str!("input.txt");

fn guard(p: bool) -> Option<()> {
    if p {
        Some(())
    } else {
        None
    }
}

fn is_hex(c: char) -> bool {
    let n = c as u16;
    (97 <= n && n <= 102) || (48 <= n && n <= 57)
}

fn discrepancy(literal: &str) -> Option<usize> {
    let mut pos = literal.chars().peekable();

    guard(pos.next()? == '"')?;

    let mut discrepancy = 1usize;

    loop {
        let c0 = pos.next()?;
        match c0 {
            '"' => {
                guard(pos.next().is_none());
                discrepancy += 1;
                break;
            }
            '\\' => match pos.next()? {
                '\\' => discrepancy += 1,
                '"' => discrepancy += 1,
                'x' => {
                    let c2 = pos.next()?;
                    let c3 = pos.next()?;
                    if is_hex(c2) && is_hex(c3) {
                        discrepancy += 3;
                    } else {
                        None?;
                    }
                }
                _ => None?,
            },
            _ => {}
        }
    }
    Some(discrepancy)
}

fn discrepancy2(literal: &str) -> usize {
    2 + literal.chars().filter(|c| *c == '"' || *c == '\\').count()
}

fn main() {
    println!(
        "Part 1: {:?}",
        INPUT.trim().lines().flat_map(discrepancy).sum::<usize>()
    );
    println!(
        "Part 2: {}",
        INPUT.trim().lines().map(discrepancy2).sum::<usize>()
    );
}
