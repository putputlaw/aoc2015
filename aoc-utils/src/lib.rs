pub fn explode<'a, T>(
    arr: &'a [T],
) -> impl Iterator<Item = (&'a[T], &'a T, &'a[T])> + 'a {
    (0..arr.len()).map(move |i| (&arr[0..i], &arr[i], &arr[i + 1..]))
}
